const operadores = require('../src/index');

describe('Testa o operador soma', function() {

    it('Testa que 5 + 6 = 11', function() {
        expect(operadores.soma(5, 6)).toBe(11);
    });

});
